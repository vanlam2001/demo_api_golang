package app

type Todo struct {
    ID   int
    Task string
}

// TodoRepository là một giao diện để quản lý danh sách công việc
type TodoRepository interface {
    Create(todo *Todo) (*Todo, error)
    GetAll() ([]*Todo, error)
}

// TodoUseCase định nghĩa các use case liên quan đến quản lý danh sách công việc
type TodoUseCase interface {
    CreateTodo(task string) (*Todo, error)
    GetTodoList() ([]*Todo, error)
}

// todoUseCase là một thực thi của TodoUseCase
type todoUseCase struct {
    todoRepository TodoRepository
}

// NewTodoUseCase tạo một thực thi TodoUseCase mới
func NewTodoUseCase(repo TodoRepository) TodoUseCase {
    return &todoUseCase{
        todoRepository: repo,
    }
}

func (uc *todoUseCase) CreateTodo(task string) (*Todo, error) {
    // Validation and business logic can be implemented here
    // Create a new todo item
    todo := &Todo{
        Task: task,
    }
    
    // Call the repository to store the todo
    createdTodo, err := uc.todoRepository.Create(todo)
    if err != nil {
        return nil, err
    }
    
    return createdTodo, nil
}

func (uc *todoUseCase) GetTodoList() ([]*Todo, error) {
    // Business logic for getting the list of todos can be implemented here
    
    // Call the repository to fetch the list of todos
    todos, err := uc.todoRepository.GetAll()
    if err != nil {
        return nil, err
    }
    
    return todos, nil
}
