// Trong file repository/todo_repository.go

package repository

import (
    "golang/internal/app"
    "sync"
)

// TodoRepository là một giao diện để quản lý danh sách công việc
type TodoRepository interface {
    Create(todo *app.Todo) (*app.Todo, error)
    GetAll() ([]*app.Todo, error)
}

// todoRepository là một thực thi của TodoRepository
type todoRepository struct {
    todos  []*app.Todo
    lastID int
    mu     sync.Mutex
}

// NewTodoRepository tạo một thực thi TodoRepository mới
func NewTodoRepository() TodoRepository {
    return &todoRepository{
        todos:  make([]*app.Todo, 0),
        lastID: 0,
    }
}

func (r *todoRepository) Create(todo *app.Todo) (*app.Todo, error) {
    r.mu.Lock()
    defer r.mu.Unlock()

    r.lastID++
    todo.ID = r.lastID
    r.todos = append(r.todos, todo)
    return todo, nil
}

func (r *todoRepository) GetAll() ([]*app.Todo, error) {
    r.mu.Lock()
    defer r.mu.Unlock()

    return r.todos, nil
}
