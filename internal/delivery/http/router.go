package http

import (
    "github.com/gin-gonic/gin"
    "golang/internal/app"
)

func SetupRouter(engine *gin.Engine, usecase app.TodoUseCase) {
    handler := NewTodoHandler(usecase)
    todoGroup := engine.Group("/todos")
    {
        todoGroup.POST("/", handler.CreateTodo)
        todoGroup.GET("/", handler.GetTodoList)
    }
}
