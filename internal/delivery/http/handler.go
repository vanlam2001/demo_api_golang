package http

import (
    "github.com/gin-gonic/gin"
    "golang/internal/app"
)

type TodoHandler struct {
    TodoUsecase app.TodoUseCase
}

func NewTodoHandler(usecase app.TodoUseCase) *TodoHandler {
    return &TodoHandler{
        TodoUsecase: usecase,
    }
}

func (h *TodoHandler) CreateTodo(c *gin.Context) {
    // Handle HTTP request to create a new todo
    // Call h.TodoUsecase.CreateTodo() and respond to the request
}

func (h *TodoHandler) GetTodoList(c *gin.Context) {
    // Handle HTTP request to get the list of todos
    // Call h.TodoUsecase.GetTodoList() and respond to the request
}
