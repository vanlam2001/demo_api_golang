package main

import (
    "github.com/gin-gonic/gin"
    "golang/internal/app"
    "golang/internal/delivery/http"
    "golang/internal/repository"
)

func main() {
    r := gin.Default()

    // Initialize and inject dependencies
    todoRepo := repository.NewTodoRepository()
    todoUsecase := app.NewTodoUseCase(todoRepo)
    http.SetupRouter(r, todoUsecase)

    r.Run(":8080")
}
